<!DOCTYPE html>
<html>
	<head>
		<title>！！！Calculator！！！</title>
	</head>
	<body>
		<?php
			$denominator_is_zero = false;
			$operator = " ";

			if (isset($_GET['num1'])) {
				$n1_is_set = true;
				if ($_GET['num1'] != '') {
					if (is_numeric($_GET['num1'])) {
			    		$n1 = (float)$_GET['num1'];	    		
			    		$n1_is_numeric = true;
			    	}
			    	else {
			    		$n1_is_numeric = false;
			    	}
			    	$n1_is_empty = false;
			    }
			    else {
			    	$n1_is_empty = true;
			    	$n1_is_numeric = false;
			    }
			}
			else {
				$n1_is_set = false;
				$n1_is_empty = true;
		    	$n1_is_numeric = false;	
			}
			
			if (isset($_GET['num2'])) {
				$n2_is_set = true;
				if ($_GET['num2'] != '') {
					if (is_numeric($_GET['num2'])) {
						$n2 = (float)$_GET['num2'];					
			    		$n2_is_numeric = true;
					}
					else {
						$n2_is_numeric = false;
					}					
					$n2_is_empty = false;
				}
				else {
					$n2_is_empty = true;
					$n2_is_numeric = false;
				}
			}      
			else {
				$n2_is_set = false;
				$n2_is_empty = true;
				$n2_is_numeric = false;
			}

			if ( $n1_is_numeric && $n2_is_numeric && isset($_GET['options']) && $_GET['options'] != '') {
				$operator = $_GET['options'];
				switch ($_GET['options']) {
					case "+":
						$result = $n1 + $n2;
						break;
					case "-":
						$result = $n1 - $n2;
						break;
					case "*":
						$result = $n1 * $n2;
						break;
					case "/":
						if ($n2 != 0) {
							$result = $n1 / $n2; 
						}
						else {
							$denominator_is_zero = true;
						}
						break;
				}
			}
		?>
		<form action ="<?php echo htmlentities($_SERVER['PHP_SELF']); ?>" method = "GET">
		<table>
			<tr>				
				<th>First Number</th>
				<th></th>
				<th>Second Number</th>
				<th></th>
			</tr>
			<tr>
				<td>
					<input type = "text" name = "num1" value = 
					<?php
					if (!$n1_is_empty && ($n2_is_empty || !$n2_is_numeric) && $n1_is_numeric) {
						echo $n1;
					}
					else {
						echo "\"\"";
					}
					?>>
				</td>
				<td>
					<input type = "radio" name = "options" value = "+" checked>+
					<input type = "radio" name = "options" value = "-">-
					<input type = "radio" name = "options" value = "*">*
					<input type = "radio" name = "options" value = "/">/
				</td>
				<td>
					<input type = "text" name= "num2" value = 
					<?php
					if (($n1_is_empty || !$n1_is_numeric) && !$n2_is_empty && $n2_is_numeric) {
						echo $n2;
					}
					else {
						echo "\"\"";
					}
					?>>
				</td>
				<td><input type="submit" value="Calculate"/></td>
			</tr>					
			<tr>
				<td colspan = "4">
				<?php
					if (isset($result)) {
					    echo "<b>Result: ".$n1." ".$operator." ".$n2." = ".$result."</b>";
					}
					else {
						if ($n1_is_set && $n2_is_set) {
							echo "<p><b>Error Message: </b></p>";
							if ($denominator_is_zero) {
								echo "<p>Denominator cannot be 0.</p>";
							} 
							if ($n1_is_empty) {
								echo "<p>Please input the first number.</p>";
							}
							else {
								if (!$n1_is_numeric) {
									echo "<p>The input for the first number is not a number.</p>";
								}
							}
							if ($n2_is_empty) {
								echo "<p>Please input the second number.</p>";
							}
							else {
								if (!$n2_is_numeric) {
									echo "<p>The input for the second number is not a number.</p>";
								}
							}
						}
					}
				?>
				</td>
			</tr>
		</table>
		</form>	
	</body>	
</html>